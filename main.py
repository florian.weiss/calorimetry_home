#####################################################################
# Kalorimetrie Küchenstisch
# Florian Weiß, 2894395, Gruppe 99
# 14.11.2023
#####################################################################

from functions import m_json
from functions import m_pck

# Heat Capacity Measurement
path = "/home/pi/calorimetry_home/datasheets/setup_newton.json" # path to subsequent setup file
metadata = m_json.get_metadata_from_setup(path) # initiate metadata for experiment

m_json.add_temperature_sensor_serials("/home/pi/calorimetry_home/datasheets/", metadata)
print(metadata) # print as check

#Measurement
data = m_pck.get_meas_data_calorimetry(metadata) # save measurement as data

# Saving Process
m_pck.logging_calorimetry(data, metadata, "/home/pi/calorimetry_home/data/", "/home/pi/calorimetry_home/datasheets/" ) # save to h5 in data-folder
m_json.archiv_json("/home/pi/calorimetry_home/datasheets/", path, "/home/pi/calorimetry_home/data/") # archive json into data-folder





